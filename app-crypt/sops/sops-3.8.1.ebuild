# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit go-module

SRC_URI="https://github.com/getsops/sops/tarball/47d295c3b662449b4ee664f13a3b6c665e18b011 -> sops-3.8.1-47d295c.tar.gz
https://direct.funtoo.org/79/33/72/7933722001226c999e9d1cec0564b0c9e941f77fff97c58f07c1af1b87cd7eb917f515ddc3cfb399ea66d08e39f321841d1a3a1b8bad04db80978a96688cba5b -> sops-3.8.1-funtoo-go-bundle-5cf875ea4accd8e338228512ae992b7c7ede9bb44d32084f20ffbf3d90b061ebc99ca03ad1e3c0871ea25ff2efb7cc3890461b9d0860ae998646a6cdb3204eaa.tar.gz"
KEYWORDS="*"

DESCRIPTION="Simple and flexible tool for managing secrets"
HOMEPAGE="https://github.com/getsops/sops"
LICENSE="MPL-2.0"
SLOT="0"
S="${WORKDIR}/getsops-sops-47d295c"

DOCS=( {CHANGELOG,README}.rst )

src_compile() {
	CGO_ENABLED=0 \
		go build -v -ldflags "-s -w" -o "${PN}" ./cmd/sops
}

src_install() {
	einstalldocs
	dobin ${PN}
}